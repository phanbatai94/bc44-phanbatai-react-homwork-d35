import './App.css';
import Ex_ShoeShopRedux from './Ex_ShoeShopRedux/Ex_ShoeShopRedux';

function App() {
  return (
    <div className='container'>
      <Ex_ShoeShopRedux />
    </div>
  );
}

export default App;
