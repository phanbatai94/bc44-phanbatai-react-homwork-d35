import React, { Component } from "react";
import { connect } from "react-redux";
import {
  changeQuantityAction,
  deleteCartAction,
} from "../redux/actions/shoeAction";

class CartShoe extends Component {
  render() {
    let { cart, handleRemove, handleQuantity } = this.props;

    return (
      <div className="col-6">
        <h2>Cart</h2>
        <table className="table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Quantity</th>
              <th>Amout</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {cart.map((item) => {
              return (
                <tr key={item.id}>
                  <td>{item.name}</td>
                  <td>
                    <span
                      onClick={() => {
                        handleQuantity(item, -1);
                      }}
                      className="btn btn-secondary"
                    >
                      -
                    </span>
                    <span className="btn btn-light">{item.soLuong}</span>
                    <span
                      onClick={() => {
                        handleQuantity(item, 1);
                      }}
                      className="btn btn-secondary"
                    >
                      +
                    </span>
                  </td>
                  <td>{item.price * item.soLuong} $</td>
                  <td>
                    <img style={{ width: 50 }} src={item.image} alt="shoe" />
                  </td>
                  <td>
                    <span
                      onClick={() => {
                        handleRemove(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      x
                    </span>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleQuantity: (shoe, option) => {
      dispatch(changeQuantityAction(shoe, option));
    },
    handleRemove: (idShoe) => {
      dispatch(deleteCartAction(idShoe));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
