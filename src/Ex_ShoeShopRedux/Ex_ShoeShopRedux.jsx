import React, { Component } from 'react'
import ListShoe from './ListShoe'
import CartShoe from './CartShoe'
import DetailShoe from './DetailShoe'

export default class Ex_ShoeShopRedux extends Component {
  render() {
    return (
      <div>
        <h1 className="text-center py-3">Shoe Shop Redux</h1>
        <div className="row">
            <ListShoe />
            <CartShoe />
        </div>
        <DetailShoe />
      </div>
    )
  }
}
