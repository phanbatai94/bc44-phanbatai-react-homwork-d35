import React, { Component } from "react";
import { connect } from "react-redux";
import { buyAction, viewDetailAction } from "../redux/actions/shoeAction";

class ItemShoe extends Component {
  render() {
    let { shoe, handleViewDetail, handleBuy } = this.props;

    return (
      <div className="col-4 mb-4">
        <div className="card bg-light h-100 text-left">
          <img className="card-img-top" src={shoe.image} alt={shoe.name} />
          <div className="card-body">
            <h4 className="card-title">{shoe.name}</h4>
            <p className="card-text">{shoe.price}</p>
            <button
              onClick={() => {
                handleViewDetail(shoe);
              }}
              className="btn btn-info"
            >
              Detail
            </button>
            <button onClick={() => { handleBuy(shoe) }} className="btn btn-success mx-2">Buy</button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleViewDetail: (shoe) => {
      dispatch(viewDetailAction(shoe));
    },
    handleBuy: (shoe) => {
      dispatch(buyAction(shoe));
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
