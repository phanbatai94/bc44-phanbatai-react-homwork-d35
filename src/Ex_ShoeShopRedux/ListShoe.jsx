import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item) => {
      return <ItemShoe key={item.id} shoe={item} />;
    });
  };
  render() {
    return (
      <div className="col-6">
        <h2>Shoes</h2>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
