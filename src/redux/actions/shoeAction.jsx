import { BUY, CHANGE_QUANTITY, DELETE_CART, VIEW_DETAIL } from "../constant/shoeContant";

export let viewDetailAction = (shoe) => {
  return {
    type: VIEW_DETAIL,
    payload: shoe,
  };
};

export let buyAction = (shoe) => {
  return {
    type: BUY,
    payload: shoe,
  };
};

export let changeQuantityAction = (shoe, option) => {
  return {
    type: CHANGE_QUANTITY,
    payload: { shoe, option },
  };
};

export let deleteCartAction = (idShoe) => {
  return {
    type: DELETE_CART,
    payload: idShoe,
  };
};
