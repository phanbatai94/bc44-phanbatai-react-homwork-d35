export const VIEW_DETAIL = "VIEW_DETAIL"

export const BUY = "BUY"

export const CHANGE_QUANTITY = "CHANGE_QUANTITY"

export const DELETE_CART = "DELETE_CART"