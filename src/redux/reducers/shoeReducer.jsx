import { shoeArr } from "../../Ex_ShoeShopRedux/data";
import {
  BUY,
  CHANGE_QUANTITY,
  DELETE_CART,
  VIEW_DETAIL,
} from "../constant/shoeContant";

const initialState = {
  shoeArr: shoeArr,
  detailShoe: shoeArr[0],
  cart: [],
};

export const shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case VIEW_DETAIL: {
      state.detailShoe = payload;
      return { ...state };
    }

    case BUY: {
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => item.id == payload.id);

      if (index == -1) {
        let newCartRow = { ...payload, soLuong: 1 };
        cloneCart.push(newCartRow);
      } else cloneCart[index].soLuong++;

      return { ...state, cart: cloneCart };
    }

    case CHANGE_QUANTITY: {
      let { shoe, option } = payload;

      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => item.id == shoe.id);

      cloneCart[index].soLuong += option;

      if (cloneCart[index].soLuong == 0) {
        cloneCart.splice(index, 1);
      }

      return { ...state, cart: cloneCart };
    }

    case DELETE_CART: {
      let cloneCart = state.cart.filter((item) => item.id !== payload);

      return { ...state, cart: cloneCart };
    }

    default: {
      return state;
    }
  }
};
